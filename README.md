# 德木自动化项目开放接口SDK

# 介绍

用于德木自动化对外开放接口数据交互

# 要求

- php版本：>=7.0

# 安装

```php
composer require ykxiao/dmmes
```

# 使用

```php

// secretKey 必填
// callback url 选填（业务逻辑需要时填写，详情查看SDK文档）
// options 选填（日志选项，默认写入日志）

$secretKey = 'gFBfirGATxafTeq74RAngaL74Ksdxhuy';

$options = [
    'logger' => true,
    'logs_path' => storage_path('logs/mes-sdk'),
];

$w = new Order($secretKey, 'callback url', $options);

// 获取工单数据
$response = $w->getProductOrder(1);

// 加工工单原木材种
$w->getWoodTypeOptions([
    'company_id'     => 1
    ]);

// 加工工单加工等级
$w->getProductionLevelOptions([
    'company_id'     => 1
    ]);

// 创建加工工单
$response = $w->createProductOrder([
    'company_id'     => 1,
    'wood_type_id'   => 1,
    'wood_level_id'  => 1,
    'spec_thickness' => 12,
    'spec_width'     => 22,
    'spec_length'    => 38,
    'number_width'   => 12,
    'number_height'  => 22,
    'number_pcs'     => 10,
    'owner'          => 'ykxiao'
]);

// 更新加工工单
$response = $w->updateProductOrder([
    'id'             => 1,
    'company_id'     => 1,
]);

// 撤回加工工单
$response = $w->revokeProductOrder([
    'id'             => 1
]);
```

# 回调说明

- 配置回调后地址后，应用服务器会以POST方式请求你的服务器地址，以便你做进一步业务流程。
- 回调你服务器地址后，可获取参数"data"、"signature"进行验签，"response" 为服务器处理结果。

```php
// 回调参数调用方法
$w = new Order($secretKey, 'callback url');

// 获取数据
$request = require();
$params = json_decode($request->contents(), true);

// 回调验签
$receivedSignature = $request->headers('mes-open-signature');
$timestamp = $request->headers('mes-open-timestamp');
$data = json_encode($params['data']);
/**
 * 生成HMAC签名
 * @param $data
 * @return string
 */
function generateHmacSignature($data): string
{
    $secretKey = 'YOUR_SECRET_KEY'; // 应用服务器密钥
    return hash_hmac('sha256', $data, $secretKey);
}

$calculatedSignature = $this->generateHmacSignature($timestamp . $data);

// 验证签名是否匹配 hash_equals($receivedSignature, $calculatedSignature)
if ($receivedSignature === $calculatedSignature) {
    return json_encode($data);
} else {
    throw new Exception('数据签名验证失败！');
}

```
