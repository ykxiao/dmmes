<?php
/**
 * Author: ykxiao
 * Date: 2023/9/7
 * Time: 09:06
 * Description: 根异常类
 */

namespace Ykxiao\Dmmes\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{

}
