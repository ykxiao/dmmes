<?php
/**
 * Author: ykxiao
 * Date: 2023/9/6
 * Time: 20:01
 * Description: 工单
 */

namespace Ykxiao\Dmmes\OrderActions;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ykxiao\Dmmes\Exceptions\Exception;
use Ykxiao\Dmmes\Exceptions\HttpException;

class Order
{
    protected const API_URL = 'https://api.dev.dwoodauto.com/api/v3/';
    protected const DEFAULT_FORMAT = 'json';
    protected const DEFAULT_TYPE = 'base';

    protected $guzzleOptions = [];
    protected $httpClient;

    protected $secretKey;
    protected $callback;
    protected $options = [];

    public function __construct($secretKey, $callback = '', $options = [])
    {
        $this->secretKey = $secretKey;
        $this->httpClient = new Client($this->guzzleOptions);
        $this->callback = $callback;
        $this->options = $options;
    }

    public function getHttpClient(): Client
    {
        return $this->httpClient;
    }

    /**
     * 请求参数签名
     * @param $params
     * @return string
     */
    private function generateSignature($params): string
    {
        return (new Sign())->generateHmacSignature($this->secretKey, $params);
    }

    /**
     * 日志
     * @param array $logs
     * @return void
     * @throws Exception
     */
    private function logger(array $logs)
    {
        $logFolder = './storage/logs/mes-sdk'; // 默认日志文件夹路径
        $logFile = $logFolder . '/logs.log'; // 默认日志文件路径

        // 如果有自定义日志路径，则使用自定义路径
        if (!empty($this->options['logger']) && !empty($this->options['logs_path'])) {
            $logFolder = $this->options['logs_path'];
            $logFile = $logFolder . '/logs.log';
        }

        // 判断文件夹是否存在，如果不存在则创建文件夹
        if (!is_dir($logFolder)) {
            if (!mkdir($logFolder, 0777, true)) {
                throw new Exception("Failed to create log folder: $logFolder");
            }
        }

        $timestamp = date('Y-m-d H:i:s');
        $logContent = "[$timestamp] " . json_encode($logs, JSON_UNESCAPED_UNICODE) . PHP_EOL;

        // 打开日志文件并追加内容
        if (file_put_contents($logFile, $logContent, FILE_APPEND) === false) {
            throw new Exception("Failed to write to log file: $logFile");
        }
    }

    /**
     * 接口请求
     * @param $api
     * @param $data
     * @return mixed|string
     * @throws \Exception|GuzzleException
     */
    private function sendRequest($api, $data)
    {
        $url = self::API_URL . $api;

        // 附加参数
        $params = array_merge([
            'times' => time(),
            'output' => self::DEFAULT_FORMAT,
            'extensions' => self::DEFAULT_TYPE,
        ], $data);

        $signature = $this->generateSignature($params);

        // 请求参数
        $body = [
            'signature' => $signature,
            'data' => $params
        ];
        if ($this->callback != '') {
            $body = array_merge(['callback' => $this->callback], $body);
        }
        $params = [
            'headers' => ['content-type' => 'application/json', 'accept' => 'application/json', 'user-agent' => 'mes sdk'],
            'body' => json_encode($body),
            'http_errors' => false
        ];

        try {
            $response = $this->httpClient
                ->request('POST', $url, $params)
                ->getBody()
                ->getContents();

            $result = 'json' === self::DEFAULT_FORMAT ? json_decode($response, true) : $response;
            // 日志
            $this->logger(['request' => $body, 'response' => $result]);
            return $result;
        } catch (\Exception $e) {
            $this->logger(['request' => $body, 'response' => $e]);
            throw new HttpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * 获取工单信息
     * @param $order_sn
     * @return mixed|string
     * @throws GuzzleException
     */
    public function getProductOrder($orderId)
    {
        $data = [
            'id' => $orderId
        ];
        return $this->sendRequest('production.info', $data);
    }

    /**
     * 加工工单原木材种
     * @return mixed|string
     * @throws GuzzleException
     */
    public function getWoodTypeOptions($data)
    {
        return $this->sendRequest('Wood.types', $data);
    }

    /**
     * 加工等级
     * @return mixed|string
     * @throws GuzzleException
     */
    public function getProductionLevelOptions($data)
    {
        return $this->sendRequest('production.levels', $data);
    }

    /**
     * 加工工单创建
     * @return mixed|string
     * @throws GuzzleException
     */
    public function createProductOrder($data)
    {
        return $this->sendRequest('production.create', $data);
    }

    /**
     * 加工工单更新
     * @return mixed|string
     * @throws HttpException|GuzzleException
     */
    public function updateProductOrder($data)
    {
        return $this->sendRequest('production.update', $data);
    }

    /**
     * 加工工单撤回
     * @return mixed|string
     * @throws HttpException|GuzzleException
     */
    public function revokeProductOrder($data)
    {
        return $this->sendRequest('production.revoke', $data);
    }
}
