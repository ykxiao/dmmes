<?php
/**
 * Author: ykxiao
 * Date: 2023/9/7
 * Time: 10:42
 * Description:
 */

namespace Ykxiao\Dmmes\OrderActions;

class Sign
{
    /**
     * 生成HMAC签名
     * @param $secretKey
     * @param $data
     * @return string
     */
    public function generateHmacSignature($secretKey, $data)
    {
        return hash_hmac('sha256', json_encode($data), $secretKey);
    }
}
