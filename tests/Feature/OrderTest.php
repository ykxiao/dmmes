<?php
/**
 * Author: ykxiao
 * Date: 2023/9/6
 * Time: 19:49
 * Description: 工单测试类
 */

namespace Feature;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Mockery;
use PHPUnit\Framework\TestCase;
use Ykxiao\Dmmes\OrderActions\Order;

class OrderTest extends TestCase
{
    /**
     * @return void
     * @throws GuzzleException
     */
    public function testGetProductOrder()
    {
        $response = new Response(200, [], '{"success": true}');

        // 创建模拟 http client。
        $client = Mockery::mock(Client::class);

        $client->allows()->post('https://api.dev.dwoodauto.com/api/v3/production.status', [
            'signature' => 'eretgfdsa34565432b453',
            'data' => ['order_sn' => 'w45676543456']
        ])->andReturn($response);

        $w = Mockery::mock(Order::class, ['mock-key'])->makePartial();

        $w->allows()->getHttpClient()->andReturn($client); // $client 为上面创建的模拟实例。

        $this->assertSame('', '');
    }
}
